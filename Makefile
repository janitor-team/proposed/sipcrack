CC 		= gcc
FLAGS 		= -O2 #-DDEBUG -g -Wall -pedantic
LIBS		= 
LIBSSL		= -lcrypto
LIBPCAP		= -lpcap
SIPCRACK	= SIPcrack.c
SIPDUMP         = SIPdump.c
OBJS		= wrap.o debug.o
MD5		= md5.o

.PHONY:		default no-openssl clean

.c.o:
		${CC} ${FLAGS} -c $< 

default:	${SIPCRACK} ${SIPDUMP} ${OBJS}
		${CC} ${FLAGS} -o sipcrack ${OBJS} ${SIPCRACK} ${LIBS} ${LIBSSL}
		${CC} ${FLAGS} -o sipdump  ${OBJS} ${SIPDUMP} ${LIBS} ${LIBPCAP}
		strip sipcrack sipdump
		@echo \* All done

no-openssl:	${SIPCRACK} ${SIPDUMP} ${OBJS} ${MD5}
		${CC} ${FLAGS} -o sipcrack -DNO_OPENSSL ${OBJS} ${MD5} ${SIPCRACK} ${LIBS}
		${CC} ${FLAGS} -o sipdump  ${OBJS} ${SIPDUMP} ${LIBS} ${LIBPCAP}
		strip sipcrack sipdump
		@echo \* All done

clean:
		rm -f sipdump sipcrack *.o

SIPcrack.o:	global.h wrap.h
SIPdump.o:	global.h wrap.h

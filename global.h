/*
 * Copyright (C) 2007  Martin J. Muench <mjm@codito.de>
 */

#ifndef SIPCRACK_GLOBAL_H
#define SIPCRACK_GLOBAL_H

#define VERSION             "0.2"        /* sipdump/sipcrack version      */
#define DEFAULT_PCAP_FILTER "tcp or udp" /* default packet capture filter */

/* sip field sizes */
#define HOST_MAXLEN       256    /* Max len of hostnames      */
#define USER_MAXLEN       128    /* Max len of user names     */
#define URI_MAXLEN        256    /* Max len of uri            */
#define NONCE_MAXLEN      128    /* Max len of nonce value    */
#define CNONCE_MAXLEN     128    /* Max len for cnonce value  */
#define NONCECOUNT_MAXLEN   8    /* Max len for nonce count   */
#define QOP_MAXLEN         12    /* Max len for qop value     */
#define LOGIN_MAXLEN     1024    /* Max len of login entry    */
#define ALG_MAXLEN          8    /* Max len of algorithm name */
#define METHOD_MAXLEN      16    /* Max len of method string  */

/* Hash stuff */
#define MD5_LEN            16    /* Len of MD5 binary hash    */
#define MD5_LEN_HEX        32    /* Len of MD5 hex hash       */
#define PW_MAXLEN          32    /* Max len of password       */

#define DYNAMIC_HASH_SIZE USER_MAXLEN + HOST_MAXLEN + 3 
#define STATIC_HASH_SIZE  NONCE_MAXLEN + CNONCE_MAXLEN + NONCECOUNT_MAXLEN \
                          + QOP_MAXLEN + MD5_LEN_HEX + 6

/* Structure to hold login information */
typedef struct {
  char server[HOST_MAXLEN];
  char client[HOST_MAXLEN];
  char user[USER_MAXLEN];
  char realm[HOST_MAXLEN];
  char method[METHOD_MAXLEN];
  char uri[URI_MAXLEN];
  char nonce[NONCE_MAXLEN];
  char cnonce[NONCE_MAXLEN];
  char nonce_count[CNONCE_MAXLEN];
  char qop[QOP_MAXLEN];
  char algorithm[ALG_MAXLEN];
  char hash[MD5_LEN_HEX+1];
} login_t;


#define SAFE_DELETE(x) if(x) free(x);

#endif /* SIPCRACK_GLOBAL_H */

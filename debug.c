/*  
 * Copyright (C) 2007  Martin J. Muench <mjm@codito.de>
 *
 * Debug function that is activated through "-DDEBUG" switch
 */

#include "debug.h"

void 
ic_debug(const char *fmt, ...)
{
  char buffer[4096];
  va_list ap;
  
  memset(buffer, 0, sizeof(buffer));
  va_start(ap, fmt);
   vsnprintf(buffer, sizeof(buffer)-1, fmt, ap);
  va_end(ap);

  fprintf(stderr, "+ %s\n", buffer);
}

/* Copyright (C) 2007  Martin J. Muench <mjm@codito.de> */

#ifndef SIPCRACK_PCACSTUFF_H
#define SIPCRACK_PCAPSTUFF_H

/* pcap stuff */
#define SNAP_LEN        1518
#define SIZE_ETHERNET   14
#define ETHER_ADDR_LEN  6

/* Ethernet header */
struct ethernet_header {
  unsigned char  ether_dhost[ETHER_ADDR_LEN];
  unsigned char  ether_shost[ETHER_ADDR_LEN];
  unsigned short ether_type;
};

/* IP header */
struct ip_header {
  unsigned char  ip_vhl;
  unsigned char  ip_tos;
  unsigned short ip_len;
  unsigned short ip_id;
  unsigned short ip_off;
#define IP_RF 0x8000            /* reserved fragment flag */
#define IP_DF 0x4000            /* dont fragment flag */
#define IP_MF 0x2000            /* more fragments flag */
#define IP_OFFMASK 0x1fff       /* mask for fragmenting bits */
  unsigned char  ip_ttl;
  unsigned char  ip_p;
  unsigned short ip_sum;
  struct  in_addr ip_src,ip_dst;
};

#define IP_HL(ip)               (((ip)->ip_vhl) & 0x0f)
#define IP_V(ip)                (((ip)->ip_vhl) >> 4)

/* TCP header */
typedef unsigned int tcp_seq;


struct tcp_header {
  unsigned short th_sport;
  unsigned short th_dport;
  tcp_seq th_seq;
  tcp_seq th_ack;
  unsigned char  th_offx2;
#define TH_OFF(th)      (((th)->th_offx2 & 0xf0) >> 4)
  unsigned char  th_flags;
#define TH_FIN  0x01
#define TH_SYN  0x02
#define TH_RST  0x04
#define TH_PUSH 0x08
#define TH_ACK  0x10
#define TH_URG  0x20
#define TH_ECE  0x40
#define TH_CWR  0x80
#define TH_FLAGS        (TH_FIN|TH_SYN|TH_RST|TH_ACK|TH_URG|TH_ECE|TH_CWR)
  unsigned short th_win;
  unsigned short th_sum;
  unsigned short th_urp;
};

struct udp_header {
  u_int16_t       uh_sport;
  u_int16_t       uh_dport;
  u_int16_t       uh_ulen;
  u_int16_t       uh_sum;
};



#endif /* SIPCRACK_PCAPSTUFF_H */


